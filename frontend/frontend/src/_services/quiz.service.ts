import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Question } from 'src/_models/question';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class QuizService {

  constructor(private http: HttpClient) { }

  generateQuiz(difficulty, category) : Observable<string> {
    return this.http.get(`http://127.0.0.1:5000/api/quiz/${difficulty}/${category}`, {responseType: "text"});
  }

  getQuestion(quizID) : Observable<Question>{
    return this.http.get<Question>(`http://127.0.0.1:5000/api/question/${quizID}`);
  }

  evaluate(quizId, questionId, answer) : Observable<any> {
   const formData = new FormData(); 
   formData.append('selectedAnswer', answer.text);
   formData.append('quizId', quizId); 
   formData.append('questionId', questionId); 

		return this.http.post(`http://127.0.0.1:5000/api/question/evaluate`, formData, {responseType: "text"});
  }
}
