import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Answer } from 'src/_models/answer';
import { QuizService } from 'src/_services/quiz.service';
import {Question} from '../../_models/question';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  @Input() quizId: string; 
  mode: string = "game"; 
  
  question: Question; 
  selectedAnswer: Answer; 

  @Output() 
		updatePointsEvent = new EventEmitter<Question>(); 

    @Output() 
		nextQuestionEvent = new EventEmitter(); 


  constructor(private quizService: QuizService) {}

  ngOnInit(): void {
    this.quizService.getQuestion(this.quizId).subscribe(q => {
      this.question = new Question(q); 
    });
  }

  selectAnswer(answer: Answer) {
    this.selectedAnswer = answer; 
  }

  evaluate() {
    this.quizService.evaluate(this.quizId, this.question.id, this.selectedAnswer).subscribe(async result => {
      await this.question.ConvertResultToBoolean(result);
      await this.question.calcAcquiredPoints(); 
      this.mode = "evaluation"; 
      this.updatePointsEvent.emit(this.question); 
    });
  }

  next() {
      this.quizService.getQuestion(this.quizId).subscribe(q => {
      this.question = new Question(q); 
      this.mode = "game"; 
      this.selectedAnswer = null; 
      this.nextQuestionEvent.emit(); 
    }); }
}
