import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from 'src/_models/question';
import { Quiz } from '../../_models/quiz'
import { QuizService } from '../../_services/quiz.service'
 

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
  quiz: Quiz; 

  counter: number = 0; 
  mode: string; 
 
  constructor(private route: ActivatedRoute, private quizService: QuizService, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.quizService.generateQuiz(params['difficulty'], params['category']).subscribe(quizId => { //gets quiz Id
        this.quiz = new Quiz({"id": quizId, "category": params['category'], "difficulty": params['difficulty']}); 
        this.mode = "game";
      }); 
    });
  }

  updatePoints(questionBefore: Question) {
    this.quiz.acquiredPoints += questionBefore.acquiredPoints;
    if(this.counter == 9) {
      this.mode = "finished";
    }
  } 

  next() {
    this.counter++; 
  }

}
