import { Component, OnInit } from '@angular/core';
import { Difficulty } from '../../_models/difficulty';
import { Category } from '../../_models/category';
import { Router } from '@angular/router';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {
  difficulties = Object.values(Difficulty);
  categories = Object.values(Category)

  selectedDifficulty: Difficulty; 
  selectedCategory: Category; 

  constructor(private router: Router) { }

  ngOnInit(): void {}

  generateQuiz() {
    this.router.navigate([`/quiz/${this.selectedDifficulty}/${this.selectedCategory}`]); 
  }

  changeDifficulty(value: string) {
    this.selectedDifficulty = Difficulty[value]; 
  }
  
  changeCategory(value: string) {
    this.selectedCategory = Category[value]; 
  }
}
