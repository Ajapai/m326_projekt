export class Quiz {
    id: string;
    category: string; 
    difficulty: string; 

    totalPoints: number; 
    acquiredPoints: number; 
    
    constructor(data: any) {
        data = data || {};
        this.id = data.id;
        this.category = data.category; 
        this.difficulty = data.difficulty; 
       
        switch (this.difficulty) {
            case 'easy':
                this.totalPoints = 10;
                break;
            case 'medium':
                this.totalPoints = 20;
                break;
            case 'hard':
                this.totalPoints = 30; 
                break;
            case 'mixed': 
                this.totalPoints = 20; 
                break;
            default:
                break;
        }
        this.acquiredPoints = 0; 
    }
}