import { Answer } from './answer';

export class Question {
    id: number;
    question: string;
    category: string; 
    difficulty: string;  
    answers: Answer[];

    result: boolean; 
    points: number;
    acquiredPoints: number; 

    constructor(data: any) {
        data = data || {};
        this.id = data.id;
        this.question = this.decodeHtml(data.question); 
        this.category = data.category; 
        this.difficulty = data.difficulty; 
        this.answers = [];
        data.answers.forEach(o => {
            this.answers.push(new Answer({"id": 0, "questionId" : data.id, "text": this.decodeHtml(o)}));
        });

        this.result = false; 

        switch (this.difficulty) {
            case 'easy':
                this.points = 1; 
                break;

            case 'medium':
                this.points = 2; 
                break;

            case 'hard':
                this.points = 3; 
                break;

            default:
                this.points = 0; 
                break;
        }
        this.acquiredPoints = 0; 
    }
    
    calcAcquiredPoints() {
        if(this.result == true) {
            this.acquiredPoints = this.points; 
        } else {
            this.acquiredPoints = 0; 
        }
    }

    ConvertResultToBoolean(result) {
        switch (result) {
            case 'False' || 0 || false:
                this.result = false; 
                break;

            case 'True' || 1 || true:
                this.result = true; 
                break;

            default:
                this.result = null; 
                break;
        }
    }

    decodeHtml(text) {
        var txt = document.createElement("textarea");
        txt.innerHTML = text;
        return txt.value;
    }
}