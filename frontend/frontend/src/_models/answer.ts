export class Answer {
    id: number;

    questionId: number;
    quizId: string; 
    
    text: string;

    constructor(data: any) {
        data = data || {};
        this.id = data.id;
        this.questionId = data.questionId;
        this.text = data.text;
    }
}
