export enum Difficulty {
    easy = "easy", 
    medium = "medium", 
    hard = "hard", 
    mixed = "mixed"
};