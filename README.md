# M326 Projekt - Rahel Kempf, Elias Gemperle
## Anleitung zum Start der Applikation
### Erste Ausführung:
#### Backend:
1. Python >= 3 installieren
2. Poetry installieren
3. Im Terminal ausführen:
    - cd backend
    - poetry install

#### Frontend:
1. Node package manager installieren
2. Angular installieren
3. Im Terminal ausführen:
    - cd frontend
    - npm install

### Weitere Ausführungen
#### Backend:
- Im Terminal ausführen:
    - cd backend/backend/api
    - poetry run python api.py

#### Frontend:
- Im Terminal ausführen:
    - cd frontend
    - ng serve

## Anleitung zum Ausführen der Unit Tests
- Im Terminal ausführen:
    - cd backend/tests
    - poetry run pytest

