import firebase_admin
from firebase_admin import credentials, db


class Database:
    def __init__(self, key):
        firebase_admin.initialize_app(
            credentials.Certificate(key),
            {"databaseURL": "https://m326-projekt-default-rtdb.firebaseio.com/"},
        )
        self.__db = db

    def read_key(self, key):
        return self.__db.reference(f"quizzes/{key}").get()

    def read_all(self):
        return self.__db.reference("quizzes").get()

    def write(self, data):
        return self.__db.reference("quizzes").push(data).key

    def update(self, key, data):
        return self.__db.reference(f"quizzes/{key}").set(data)

    def delete(self, key):
        return self.__db.reference(f"quizzes/{key}").delete()
