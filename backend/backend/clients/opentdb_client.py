import requests


class Opentdb:
    CATEGORIES = {
        "any": "any",
        "general_knowledge": 9,
        "books": 10,
        "movies": 11,
        "music": 12,
        "musicals_and_theatres": 13,
        "television": 14,
        "video_games": 15,
        "board_games": 16,
        "science_and_nature": 17,
        "computers": 18,
        "mathematics": 19,
        "mythology": 20,
        "sports": 21,
        "geography": 22,
        "history": 23,
        "politics": 24,
        "art": 25,
        "celebrities": 26,
        "animals": 27,
        "vehicles": 28,
        "comics": 29,
        "science_gadgets": 30,
        "anime_and_manga": 31,
        "cartoon_and_animations": 32,
    }
    DIFFICULTIES = ["easy", "medium", "hard", "mixed"]
    QUESTIONS_IDS = {
        1: "1st",
        2: "2nd",
        3: "3rd",
        4: "4th",
        5: "5th",
        6: "6th",
        7: "7th",
        8: "8th",
        9: "9th",
        10: "10th",
    }

    class Url:
        def __init__(self, amount):
            self.__url = f"https://opentdb.com/api.php?amount={amount}"

        def add_category(self, category):
            self.__url += f"&category={category}"

        def add_difficulty(self, difficulty):
            self.__url += f"&difficulty={difficulty}"

        @property
        def url(self):
            return f"{self.__url}&type=multiple"

    @classmethod
    def get_questions(cls, category, difficulty):
        if category not in cls.CATEGORIES or difficulty not in cls.DIFFICULTIES:
            return None

        # Return mixed questions
        if difficulty == "mixed":
            questions = cls.__get_mixed_questions(category)

        # Return questions of a set difficulty
        else:
            questions = cls.__request_questions(10, category, difficulty).json()["results"]

        for index, question in enumerate(questions):
            question["id"] = cls.QUESTIONS_IDS[index + 1]
        return questions

    @classmethod
    def __request_questions(cls, amount, category, difficulty):
        url = cls.Url(amount)
        if category != cls.CATEGORIES["any"]:
            url.add_category(cls.CATEGORIES[category])
        url.add_difficulty(difficulty)
        return requests.get(url.url, verify=False)

    @classmethod
    def __get_mixed_questions(cls, category):
        return [
            result
            for results in [
                cls.__request_questions(
                    4 if difficulty == "medium" else 3, category, difficulty
                ).json()["results"]
                for difficulty in cls.DIFFICULTIES[0:-1]
            ]
            for result in results
        ]
