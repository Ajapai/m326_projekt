class Question:
    def __init__(
        self,
        id,
        category,
        difficulty,
        question,
        correct_answer,
        incorrect_answers,
    ):
        self.id : str = id
        self.category : str = category
        self.difficulty : str = difficulty
        self.question : str = question
        self.correct_answer : str = correct_answer
        self.answers : list(str) = sorted(incorrect_answers + [correct_answer])

    def to_db_dict(self):
        question_dict = vars(self)
        return question_dict

    def to_web_dict(self, quiz_id):
        question_dict = vars(self)
        del question_dict["correct_answer"]
        question_dict["quiz_id"] = quiz_id
        return question_dict

    @classmethod
    def from_dict(cls, question_dict) -> 'Question':
        if question_dict and cls.__dict_contains_all_keys(question_dict):
            return cls(
                question_dict["id"],
                question_dict["category"],
                question_dict["difficulty"],
                question_dict["question"],
                question_dict["correct_answer"],
                question_dict["incorrect_answers"]
                if "incorrect_answers" in question_dict
                else [
                    answer
                    for answer in question_dict["answers"]
                    if answer != question_dict["correct_answer"]
                ],
            )
        return None

    @staticmethod
    def __dict_contains_all_keys(question_dict) -> bool:
        if "incorrect_answers" not in question_dict and "answers" not in question_dict:
            return False
        return all(
            key in question_dict
            for key in [
                "id",
                "category",
                "difficulty",
                "question",
                "correct_answer",
            ]
        )
