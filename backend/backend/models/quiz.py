from datetime import datetime, timedelta

from backend.models.question import Question


class Quiz:
    def __init__(self, category, difficulty, questions, current_question, timestamp):
        self.category : str = category
        self.difficulty : str = difficulty
        self.questions : list(Question) = questions
        self.current_question : int = current_question
        self.timestamp : float = timestamp

    def is_older_than_one_hour(self):
        quiz_creation_time = datetime.fromtimestamp(self.timestamp)
        one_hour_ago = datetime.now() - timedelta(hours=1)
        return quiz_creation_time < one_hour_ago

    def to_db_dict(self):
        return {
            "category": self.category,
            "difficulty": self.difficulty,
            "questions": {key: question.to_db_dict() for (key, question) in self.questions.items()},
            "current_question": self.current_question,
            "timestamp": self.timestamp,
        }

    @classmethod
    def from_dict(cls, quiz_dict) -> 'Quiz':
        if quiz_dict is None or not cls.__dict_contains_all_keys(quiz_dict):
            return None
        questions = {
            key: Question.from_dict(question) for (key, question) in quiz_dict["questions"].items()
        }
        return (
            None
            if any(question is None for question in questions)
            else cls(
                quiz_dict["category"],
                quiz_dict["difficulty"],
                questions,
                quiz_dict["current_question"],
                quiz_dict["timestamp"],
            )
        )

    @staticmethod
    def __dict_contains_all_keys(question_dict) -> bool:
        return all(
            key in question_dict
            for key in [
                "category",
                "difficulty",
                "questions",
                "current_question",
                "timestamp",
            ]
        )
