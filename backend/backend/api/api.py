#!/usr/bin/env python3

from datetime import datetime

from flask import Flask, request
from flask_apscheduler import APScheduler
from urllib3 import disable_warnings, exceptions
from flask_cors import CORS, cross_origin

from backend.clients.firebase_client import Database
from backend.clients.opentdb_client import Opentdb
from backend.models.question import Question
from backend.models.quiz import Quiz

# Initialize app, database and scheduler
app = Flask(__name__)
CORS(app)
db = Database("../../resources/m326_firebase_key.json")

scheduler = APScheduler()
scheduler.api_enabled = True
scheduler.init_app(app)


# Define scheduler functions
@scheduler.task("cron", id="clean_db", minute="*/15")
def clean_db():
    print("Cleaning db...")
    quizzez = db.read_all()
    if quizzez is None:
        print("Nothing to clean, db is empty")
        return

    for key, quiz in quizzez.items():
        current_quiz = Quiz.from_dict(quiz)
        if current_quiz is None or current_quiz.is_older_than_one_hour():
            db.delete(key)
            print(f"Removed '{key}'")
    print("Done cleaning")


# Activates defined scheduler functions
scheduler.start()


# Define API functions
@app.route(
    f"/api/quiz/<any({','.join(Opentdb.DIFFICULTIES)}):difficulty>/"
    f"<any({','.join(Opentdb.CATEGORIES)}):category>",
    methods=["GET"],
)
@cross_origin()
def new_quiz(difficulty, category):
    quiz = Quiz(
        category,
        difficulty,
        {
            question["id"]: Question.from_dict(question)
            for question in Opentdb.get_questions(category, difficulty)
        },
        0,
        datetime.timestamp(datetime.now()),
    )
    if len(quiz.questions) < 10:
        return f"Not enough '{difficulty}' questions for '{category}'"
    key = db.write(quiz.to_db_dict())
    return key


@app.route("/api/question/<quiz_id>/", methods=["GET"])
def next_question(quiz_id):
    quiz = Quiz.from_dict(db.read_key(quiz_id))
    if quiz is None:
        return "No such resource", 404

    if quiz.current_question < 10:
        quiz.current_question += 1
        db.update(quiz_id, quiz.to_db_dict())
        return quiz.questions[Opentdb.QUESTIONS_IDS[quiz.current_question]].to_web_dict(quiz_id)

    return "No more Questions"


@app.route("/api/question/evaluate", methods=["POST"])
def evaluate_question():
    if any(
        parameter not in request.form for parameter in ["selectedAnswer", "quizId", "questionId"]
    ):
        return "Invalid POST parameters", 400
    answer = request.form.get("selectedAnswer")
    quiz_id = request.form.get("quizId")
    question_id = request.form.get("questionId")
    question = Question.from_dict(db.read_key(f"{quiz_id}/questions/{question_id}"))

    return str(answer == question.correct_answer)


if __name__ == "__main__":
    disable_warnings(exceptions.InsecureRequestWarning)
    app.run()
