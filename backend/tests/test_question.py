import json
import pytest

from backend.models.question import Question


@pytest.fixture()
def invalid_dict():
    with open('test_files/invalid_question.json') as json_file:
        return json.load(json_file)


@pytest.fixture()
def valid_dict():
    with open('test_files/valid_question.json') as json_file:
        return json.load(json_file)


@pytest.fixture()
def quiz_id():
    return "This_could_be_any_id"


def test_from_dict_with_invalid_dict(invalid_dict):
    # WHEN
    question = Question.from_dict(invalid_dict)

    # THEN
    assert question is None


def test_from_dict_with_valid_dict(valid_dict):
    # WHEN
    question = Question.from_dict(valid_dict)

    # THEN
    assert question is not None


def test_to_db_dict_returns_same_json(valid_dict):
    # WHEN
    question = Question.from_dict(valid_dict)

    # THEN
    assert question.to_db_dict() == valid_dict


def test_to_web_dict_returns_json_without_correct_answer(valid_dict, quiz_id):
    # GIVEN
    question = Question.from_dict(valid_dict)

    # WHEN
    question_as_web_dict = question.to_web_dict(quiz_id)

    # THEN
    assert "correct_answer" not in question_as_web_dict


def test_to_web_dict_returns_json_with_quiz_id(valid_dict, quiz_id):
    # GIVEN
    question = Question.from_dict(valid_dict)

    # WHEN
    question_as_web_dict = question.to_web_dict(quiz_id)

    # THEN
    assert "quiz_id" in question_as_web_dict and question_as_web_dict["quiz_id"] == quiz_id
