import json
import pytest
from datetime import datetime, timedelta


from backend.models.quiz import Quiz


@pytest.fixture()
def invalid_dict():
    with open('test_files/invalid_quiz.json') as json_file:
        return json.load(json_file)


@pytest.fixture()
def valid_dict():
    with open('test_files/valid_quiz.json') as json_file:
        return json.load(json_file)


def test_from_dict_with_invalid_dict(invalid_dict):
    # WHEN
    quiz = Quiz.from_dict(invalid_dict)

    # THEN
    assert quiz is None


def test_from_dict_with_valid_dict(valid_dict):
    # WHEN
    quiz = Quiz.from_dict(valid_dict)

    # THEN
    assert quiz is not None


def test_to_db_dict_returns_same_json(valid_dict):
    # WHEN
    quiz = Quiz.from_dict(valid_dict)

    # THEN
    assert quiz.to_db_dict() == valid_dict


def test_is_older_than_one_hour_with_new_timestamp(valid_dict):
    # GIVEN
    quiz = Quiz.from_dict(valid_dict)

    # WHEN
    quiz.timestamp = datetime.timestamp(datetime.now())

    # THEN
    assert quiz.is_older_than_one_hour() is False


def test_is_older_than_one_hour_with_old_timestamp(valid_dict):
    # GIVEN
    quiz = Quiz.from_dict(valid_dict)

    # WHEN
    quiz.timestamp = datetime.timestamp(datetime.now() - timedelta(hours=2))

    # THEN
    assert quiz.is_older_than_one_hour() is True
